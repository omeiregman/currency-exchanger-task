export const config = {
  apiBaseUrl: 'https://openexchangerates.org/api',
  apiAccessKey: '3f12e87636a64ec89445e823e4eea63f',
  ratePollingFrequency: 10000,
};

export const apiRoute = {
  rate: 'latest.json?app_id=3f12e87636a64ec89445e823e4eea63f',
};
