import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  palette: {
    primary: { main: '#2768E3', light: '#578DF1' },
    secondary: { main: '#ACACAC', light: '#E5E5E5' },
    error: { main: '#FF4F4F' },
  },
});
