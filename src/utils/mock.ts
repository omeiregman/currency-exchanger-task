import { Account, AccountIndex, Currency } from '../types/types';
import { generateRandomBalance } from './util';
import { LoadingState } from '../types/enums';

export const mockCurrencies: Currency[] = [
  {
    currency: 'US Dollar',
    currencyCode: 'USD',
    currencySymbol: '$',
    currencyIcon: 'usd',
  },
  {
    currency: 'Euro',
    currencyCode: 'EUR',
    currencySymbol: '€',
    currencyIcon: 'eur',
  },
  {
    currency: 'British Pounds',
    currencyCode: 'GBP',
    currencySymbol: '£',
    currencyIcon: 'gbp',
  },
];

export const mockAccounts: Account[] = mockCurrencies.map((currency) => ({
  currency,
  balance: generateRandomBalance(),
}));

export const mockAccountIndex: AccountIndex = mockAccounts.reduce(
  (acc: AccountIndex, curr: Account) => {
    acc[curr.currency.currencyCode] = curr;
    return acc;
  },
  {}
);

export const mockRateApiResponse = {
  disclaimer: 'https://openexchangerates.org/terms/',
  license: 'https://openexchangerates.org/license/',
  timestamp: 1424127600,
  base: 'EUR',
  rates: {
    EUR: 4.626447,
  },
};

export const mockUpdateAccountData = {
  fromCurrency: mockCurrencies[0],
  fromBalance: 2000,
  fromAmount: 100,
  toCurrency: mockCurrencies[1],
  toBalance: 1000,
  toAmount: 85,
};

export const mockInitialStore = {
  accounts: { accounts: mockAccountIndex, loading: LoadingState.PRISTINE },
  exchange: {
    rate: 12,
    exchangeFrom: mockCurrencies[0],
    exchangeTo: mockCurrencies[1],
    loading: LoadingState.PRISTINE,
  },
};
