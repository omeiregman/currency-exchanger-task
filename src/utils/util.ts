import { RefObject } from 'react';

export const generateRandomBalance = (min = 200, max = 1000) =>
  Math.floor(Math.random() * (max - min)) + min;

export const decimalCount = (value: number) => {
  if (Math.floor(value) === value) return 0;
  return value.toString().split('.')[1].length || 0;
};

export const fixDecimalPlace = (value: number, decimal: number = 2) =>
  Number(value.toFixed(decimal));

export const isElementActive = (ref: RefObject<HTMLInputElement>) =>
  ref.current === document.activeElement;
