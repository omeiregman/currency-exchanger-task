import { decimalCount, fixDecimalPlace } from './util';

describe('Decimal count test', () => {
  it('should be true if decimal place is present', () => {
    expect(decimalCount(12.23)).toBeTruthy();
  });
  it('should be false if decimal is not present', () => {
    expect(decimalCount(12)).toBeFalsy();
  });
  it('should be 3 if decimal place count is 3', () => {
    expect(decimalCount(15.123)).toEqual(3);
  });
});

describe('Fix decimal place test', () => {
  it('should return float to 2dp as default', () => {
    expect(fixDecimalPlace(12.2312)).toEqual(12.23);
  });
  it('should return float to 4dp based on 2nd parameter', () => {
    expect(fixDecimalPlace(12.23344444, 4)).toEqual(12.2334);
  });
});
