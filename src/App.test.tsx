import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { App } from './App';
import createStore from './store/configureStore';

const store = createStore();

test('renders app ui', () => {
  const { container } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  expect(container).toBeTruthy();
});
