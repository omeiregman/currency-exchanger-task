import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Button, ButtonProps } from './Button';

export default {
  title: 'Atoms/Button',
  component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => (
  <Button {...args}>Convert</Button>
);

export const Primary = Template.bind({});
Primary.args = {
  type: 'submit',
};

export const Disabled = Template.bind({});
Disabled.args = {
  type: 'submit',
  disabled: true,
};
