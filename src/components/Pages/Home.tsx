import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Flex } from '../Flex/Flex';
import { AccountSection } from '../AccountSection/AccountSection';
import { ExchangeSection } from '../ExchangeSection/ExchangeSection';
import {
  getAccountsLoadingState,
  getAccountsSelector,
} from '../../store/accounts/selectors';
import { LoadingState } from '../../types/enums';
import { fetchAccountsAction } from '../../store/accounts/action';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      margin: theme.spacing(4, 0),
      alignItems: 'center',
    },
  })
);

export const Home: React.FC = () => {
  const dispatch = useDispatch();
  const accounts = useSelector(getAccountsSelector);
  const accountsLoadingState = useSelector(getAccountsLoadingState);
  const isLoadingAccount = accountsLoadingState === LoadingState.LOADING;

  const classes = useStyles();

  useEffect(() => {
    dispatch(fetchAccountsAction());
  }, [dispatch]);
  return (
    <Flex flexDirection="column" className={classes.root}>
      {isLoadingAccount && <div>Loading...</div>}
      {!!accounts.length && (
        <>
          <AccountSection accounts={accounts} />
          <ExchangeSection accounts={accounts} />
        </>
      )}
    </Flex>
  );
};
