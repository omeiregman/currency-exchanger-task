import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles<Theme, FlexProps>(() =>
  createStyles({
    root: {
      boxSizing: 'border-box',
      display: 'flex',
      flexDirection: ({ flexDirection = 'row' }) => flexDirection,
    },
  })
);

interface FlexProps {
  children: React.ReactNode;
  className?: string;
  flexDirection?: 'row' | 'column' | 'row-reverse' | 'column-reverse';
  testId?: string;
}

export const Flex: React.FC<FlexProps> = (props) => {
  const { children, className, testId } = props;
  const classes = useStyles(props);
  return (
    <div className={clsx(className, classes.root)} data-testid={testId}>
      {children}
    </div>
  );
};
