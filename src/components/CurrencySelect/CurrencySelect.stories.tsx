import React from 'react';
import { Story, Meta } from '@storybook/react';

import { CurrencySelect, CurrencySelectProps } from './CurrencySelect';
import { mockAccounts } from '../../utils/mock';

export default {
  title: 'Molecules/CurrencySelect',
  component: CurrencySelect,
} as Meta;

const Template: Story<CurrencySelectProps> = (args) => (
  <CurrencySelect {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  accounts: mockAccounts,
  onSelect: () => {},
  initial: mockAccounts[1].currency,
};
