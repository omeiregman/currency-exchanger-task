import React, { useState } from 'react';
import {
  Box,
  ClickAwayListener,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { Account, Currency } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { Icon } from '../Icon/Icon';
import { Button } from '../Button/Button';
import { Text } from '../Text/Text';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      position: 'relative',
    },
    button: {
      width: 100,
      height: 52,
      border: `1px solid ${theme.palette.secondary.main}`,
      padding: theme.spacing(1),
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
    wrapper: {
      alignItems: 'center',
      cursor: 'pointer',
      justifyContent: 'space-between',
      color: theme.palette.common.black,
    },
    icon: {
      marginTop: 2,
    },
    listIcon: {
      marginRight: 10,
    },
    list: {
      display: 'flex',
      flexDirection: 'column',
      boxShadow: theme.shadows[3],
      padding: theme.spacing(0),
      width: 100,
      borderRadius: 8,
      zIndex: 1,
      position: 'absolute',
      top: theme.spacing(-3),
      backgroundColor: theme.palette.common.white,
    },
    listItem: {
      padding: theme.spacing(1),
      display: 'flex',
      justifyContent: 'center',
      cursor: 'pointer',
      '&:hover': {
        '&:first-child': {
          borderTopLeftRadius: 8,
          borderTopRightRadius: 8,
        },
        '&:last-child': {
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
        },
        backgroundColor: theme.palette.secondary.light,
      },
    },
  })
);

export interface CurrencySelectProps {
  accounts: Account[];
  onSelect: (account: Account) => void;
  initial: Currency;
  testId: string;
}

export const CurrencySelect: React.FC<CurrencySelectProps> = ({
  accounts,
  initial,
  onSelect,
  testId,
}) => {
  const [showList, setShowList] = useState(false);

  const closeList = () => {
    setShowList(false);
  };
  const toggleShowList = () => {
    setShowList(!showList);
  };

  const onSelectCurrency = (account: Account) => {
    setShowList(false);
    onSelect(account);
  };

  const classes = useStyles();
  return (
    <Flex className={classes.root} testId={testId}>
      <Button
        styleType="transparent"
        label="open-list"
        type="button"
        onClick={toggleShowList}
        className={classes.button}
        testId={`${testId}-button`}
      >
        <Flex className={classes.wrapper}>
          <Icon icon={initial.currencyIcon} width={20} height={20} />
          <span>{initial.currencyCode}</span>
          <Icon
            icon="arrowDown"
            width={15}
            height={15}
            className={classes.icon}
          />
        </Flex>
      </Button>
      {showList && (
        <ClickAwayListener onClickAway={closeList}>
          <Box className={classes.list} data-testid={`${testId}-options`}>
            {accounts.map((account) => (
              <Box
                key={account.currency.currencyCode}
                data-testid={`${testId}-option`}
                role="button"
                tabIndex={-1}
                className={classes.listItem}
                onClick={() => onSelectCurrency(account)}
              >
                <Icon
                  className={classes.listIcon}
                  icon={account.currency.currencyIcon}
                  width={15}
                  height={15}
                />
                <Text fontSize={14}>{account.currency.currencyCode}</Text>
              </Box>
            ))}
          </Box>
        </ClickAwayListener>
      )}
    </Flex>
  );
};
