import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import { CurrencySelect } from './CurrencySelect';
import { mockAccounts } from '../../utils/mock';

afterEach(() => {
  cleanup();
});

describe('Currency Select test', () => {
  it('should render Account Section component', () => {
    render(
      <CurrencySelect
        accounts={mockAccounts}
        onSelect={() => {}}
        initial={mockAccounts[0].currency}
        testId="currency-select"
      />
    );
    const CurrencySelectComponent = screen.getByTestId('currency-select');
    expect(CurrencySelectComponent).toBeInTheDocument();
    expect(CurrencySelectComponent).toHaveTextContent('USD');
  });

  it('displays currency options when clicked', () => {
    render(
      <CurrencySelect
        accounts={mockAccounts}
        onSelect={() => {}}
        initial={mockAccounts[0].currency}
        testId="currency-select"
      />
    );
    const CurrencySelectButton = screen.getByTestId('currency-select-button');
    fireEvent.click(CurrencySelectButton);
    const CurrencySelectOptions = screen.getByTestId('currency-select-options');
    expect(CurrencySelectOptions).toBeInTheDocument();
  });

  it('renders all options when clicks', () => {
    render(
      <CurrencySelect
        accounts={mockAccounts}
        onSelect={() => {}}
        initial={mockAccounts[0].currency}
        testId="currency-select"
      />
    );
    const CurrencySelectButton = screen.getByTestId('currency-select-button');
    fireEvent.click(CurrencySelectButton);
    const options = screen.getAllByTestId('currency-select-option');
    expect(options.length).toEqual(mockAccounts.length);
  });
});
