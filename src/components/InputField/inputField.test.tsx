import { render, screen, cleanup } from '@testing-library/react';
import { InputField } from './InputField';

afterEach(() => {
  cleanup();
});

describe('Input Field test', () => {
  const inputProps = {
    id: 'convert',
    type: 'number',
    name: 'convert',
    placeholder: '',
    value: '123',
    label: 'Convert',
    message: 'Balance: $23,34',
    error: false,
    currency: '€',
    testId: 'exchange-from-input',
  };
  it('should render Account Section component', () => {
    render(
      <InputField
        id="input"
        name="currencyTo"
        type="number"
        label="Convert"
        placeholder=""
        currency="€"
        message="Exceeds balance"
        value="123"
        onChange={() => {}}
        error={false}
        testId="exchange-from-input"
      />
    );
    const InputFieldComponent = screen.getByTestId('test-input-field');
    expect(InputFieldComponent).toBeInTheDocument();
  });
});
