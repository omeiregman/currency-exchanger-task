import React, { RefObject } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles<Theme, InputFieldProps>((theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: ({ width = 300 }) => width,
      position: 'relative',
    },
    input: {
      boxSizing: 'border-box',
      border: ({ error }) =>
        error
          ? `1px solid ${theme.palette.error.main}`
          : `1px solid ${theme.palette.secondary.main}`,
      borderRadius: 8,
      outline: 0,
      padding: theme.spacing(1),
      paddingTop: theme.spacing(2.75),
      paddingLeft: theme.spacing(4),
      height: 52,
      '&[type=number]': {
        '-moz-appearance': 'textfield',
      },
      '&::-webkit-outer-spin-button': {
        '-webkit-appearance': 'none',
      },
      '&::-webkit-inner-spin-button': {
        '-webkit-appearance': 'none',
      },
      fontSize: 18,
      fontWeight: 500,
      color: theme.palette.common.black,
    },
    currency: {
      position: 'absolute',
      fontSize: 18,
      color: theme.palette.common.black,
      left: 10,
      top: 23,
      display: 'flex',
      justifyContent: 'center',
      alignItem: 'center',
      width: 20,
      height: 20,
      borderRadius: 2,
      backgroundColor: theme.palette.secondary.light,
    },
    message: {
      display: 'flex',
      justifyContent: 'flex-end',
      position: 'absolute',
      right: 8,
      bottom: 4,
      marginTop: 5,
      fontSize: 12,
      color: ({ error }) =>
        error ? theme.palette.error.main : theme.palette.secondary.main,
    },
    label: {
      color: theme.palette.secondary.main,
      fontSize: 12,
      position: 'absolute',
      top: 0,
      paddingLeft: theme.spacing(1.25),
      paddingTop: 4,
      left: 0,
    },
  })
);

export interface InputFieldProps {
  id: string;
  name: string;
  type: string;
  placeholder?: string;
  label: string;
  error?: boolean;
  value: string | number;
  currency: string;
  message: string;
  width?: number | string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  className?: string;
  innerRef?: RefObject<HTMLInputElement>;
  testId: string;
  inputMode?:
    | 'none'
    | 'text'
    | 'tel'
    | 'url'
    | 'email'
    | 'numeric'
    | 'decimal'
    | 'search'
    | undefined;
}

export const InputField: React.FC<InputFieldProps> = (props) => {
  const {
    id,
    name,
    type,
    placeholder,
    message,
    value,
    onChange,
    label,
    currency,
    className,
    innerRef,
    inputMode,
    testId,
  } = props;

  const classes = useStyles(props);

  return (
    <div
      className={clsx(className, classes.root)}
      data-testid="test-input-field"
    >
      <span className={classes.currency}>{currency}</span>
      <input
        ref={innerRef}
        id={id}
        className={classes.input}
        placeholder={placeholder}
        type={type}
        inputMode={inputMode}
        name={name}
        onChange={onChange}
        value={value || ''}
        aria-label={label}
        data-testid={testId}
      />
      <label className={classes.label} htmlFor={name}>
        {label}
      </label>
      <div className={classes.message}>{message}</div>
    </div>
  );
};
