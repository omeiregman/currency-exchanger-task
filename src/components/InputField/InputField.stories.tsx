import React from 'react';
import { Story, Meta } from '@storybook/react';

import { InputField, InputFieldProps } from './InputField';

export default {
  title: 'Atoms/Input',
  component: InputField,
} as Meta;

const Template: Story<InputFieldProps> = (args) => <InputField {...args} />;

export const Default = Template.bind({});
Default.args = {
  id: 'convert',
  type: 'number',
  name: 'convert',
  placeholder: '',
  value: '123',
  label: 'Convert',
  error: false,
  currency: '$',
  testId: 'exchange-from-input',
};

export const Message = Template.bind({});
Message.args = {
  id: 'convert',
  type: 'number',
  name: 'convert',
  placeholder: '',
  value: '123',
  label: 'Convert',
  message: 'Balance: $23,34',
  error: false,
  currency: '€',
  testId: 'exchange-from-input',
};

export const Error = Template.bind({});
Error.args = {
  id: 'convert',
  type: 'number',
  name: 'convert',
  placeholder: '',
  value: '123',
  label: 'Convert',
  message: 'Exceeds Balance: $23,34',
  error: true,
  currency: '€',
  testId: 'exchange-from-input',
};
