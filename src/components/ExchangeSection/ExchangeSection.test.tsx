import { render, screen, cleanup, fireEvent } from '../../utils/test-helper';
import { ExchangeSection } from './ExchangeSection';
import { mockAccounts } from '../../utils/mock';

afterEach(() => {
  cleanup();
});

describe('Exchange Section', () => {
  it('it renders Exchange section', () => {
    render(<ExchangeSection accounts={mockAccounts} />);
    const ExchangeSectionComponent = screen.getByTestId('exchange-section');
    expect(ExchangeSectionComponent).toBeInTheDocument();
    expect(ExchangeSectionComponent).toHaveTextContent('Market Order');
  });
});

describe('Exchange Input section', () => {
  const setup = () => {
    render(<ExchangeSection accounts={mockAccounts} />);
    const ExchangeSectionComponent = screen.getByTestId('exchange-section');

    const InputField = screen.getByTestId('exchange-from-input');
    return {
      InputField,
      ...ExchangeSectionComponent,
    };
  };
  it('should not allow alphabets to be inputted', () => {
    const { InputField } = setup();
    const inputValue = InputField as HTMLInputElement;
    expect(inputValue.value).toEqual('');
    fireEvent.change(InputField, { target: { value: 'sendmoney' } });
    expect(inputValue.value).toEqual('');
  });

  it('should allow numbers to be inputted', () => {
    const { InputField } = setup();
    const inputValue = InputField as HTMLInputElement;
    expect(inputValue.value).toEqual('');
    fireEvent.change(InputField, { target: { value: 12234 } });
    expect(inputValue.value).toEqual('12234');
  });

  it('should return empty if more than 2 decimal placed number is pasted', () => {
    const { InputField } = setup();
    const inputValue = InputField as HTMLInputElement;
    expect(inputValue.value).toEqual('');
    fireEvent.change(InputField, { target: { value: '123.112' } });
    expect(inputValue.value).toEqual('');
  });

  it('enables exchange button if input', () => {
    const { InputField } = setup();
    const ExchangeButton = screen.getByTestId('exchange-button');
    fireEvent.change(InputField, { target: { value: 12 } });
    expect(ExchangeButton).toBeEnabled();
  });
});

describe('Exchange currency select test', () => {
  it('selects the clicked item from the currency options', () => {
    render(<ExchangeSection accounts={mockAccounts} />);

    const CurrencySelectButton = screen.getByTestId(
      'exchange-from-select-button'
    );
    fireEvent.click(CurrencySelectButton);
    const options = screen.getAllByTestId('exchange-from-select-option');
    fireEvent.click(options[2]);
    expect(CurrencySelectButton).toHaveTextContent(
      mockAccounts[2].currency.currencyCode
    );
  });
});

describe('Exchange Button actions', () => {
  it('disables exchange button if same currency is selected', () => {
    render(<ExchangeSection accounts={mockAccounts} />);
    const ToCurrencySelect = screen.getByTestId('exchange-to-select-button');
    const ExchangeButton = screen.getByTestId('exchange-button');
    fireEvent.click(ToCurrencySelect);
    const optionsTo = screen.getAllByTestId('exchange-to-select-option');
    fireEvent.click(optionsTo[0]);
    expect(ExchangeButton).toBeDisabled();
  });

  it('disables exchange button if no input', () => {
    render(<ExchangeSection accounts={mockAccounts} />);
    const ExchangeButton = screen.getByTestId('exchange-button');
    expect(ExchangeButton).toBeDisabled();
  });
});
