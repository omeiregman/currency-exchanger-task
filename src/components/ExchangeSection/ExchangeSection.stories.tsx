import React from 'react';
import { Story, Meta } from '@storybook/react';

import { mockAccounts } from '../../utils/mock';
import { ExchangeSection, ExchangeSectionProps } from './ExchangeSection';

export default {
  title: 'Organisms/ExchangeSection',
  component: ExchangeSection,
} as Meta;

const Template: Story<ExchangeSectionProps> = (args) => (
  <ExchangeSection {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  accounts: mockAccounts,
};
