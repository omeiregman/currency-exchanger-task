import React, { useEffect, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Account, Currency } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { Text } from '../Text/Text';
import { InputField } from '../InputField/InputField';
import { CurrencySelect } from '../CurrencySelect/CurrencySelect';
import { Button } from '../Button/Button';
import {
  changeExchangeFromAction,
  changeExchangeToAction,
  startPollingRateAction,
  stopPollingRateAction,
} from '../../store/exchange/action';
import {
  decimalCount,
  fixDecimalPlace,
  isElementActive,
} from '../../utils/util';
import { updateAccountAction } from '../../store/accounts/action';
import {
  getExchangeFromSelector,
  getExchangeToSelector,
  getRateLoadingStateSelector,
  getRateSelector,
} from '../../store/exchange/selectors';
import { LoadingState } from '../../types/enums';
import { theme } from '../../utils/theme';
import {
  getFromAccountBalance,
  getToAccountBalance,
} from '../../store/accounts/selectors';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      marginTop: theme.spacing(4),
      flexDirection: 'column',
      [theme.breakpoints.down('xs')]: {
        width: 350,
        justifyContent: 'center',
      },
    },
    topInfo: {
      flexDirection: 'column',
      margin: theme.spacing(4, 0),
    },
    errorText: {
      color: theme.palette.error.main,
    },
    input: {
      marginRight: theme.spacing(1),
    },
    inputDivider: {
      alignItems: 'center',
      marginLeft: theme.spacing(1.5),
      marginBottom: 1,
      borderLeft: `1px solid ${theme.palette.secondary.main}`,
      height: 40,
    },
    rateIndicator: {
      width: 10,
      height: 10,
      marginLeft: theme.spacing(-0.625),
      marginRight: theme.spacing(1.5),
      borderRadius: 8,
      backgroundColor: theme.palette.secondary.main,
    },
    exchangeButton: {
      marginTop: theme.spacing(3),
    },
  })
);

export interface ExchangeSectionProps {
  accounts: Account[];
}

export const ExchangeSection: React.FC<ExchangeSectionProps> = ({
  accounts,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const exchangeFromCurrency = useSelector(getExchangeFromSelector);
  const exchangeToCurrency = useSelector(getExchangeToSelector);
  const exchangeFromBalance = useSelector(getFromAccountBalance);
  const exchangeToBalance = useSelector(getToAccountBalance);
  const rate = useSelector(getRateSelector);
  const rateLoadingState = useSelector(getRateLoadingStateSelector);
  const rateError = rateLoadingState === LoadingState.FAILED;
  const fromInputRef = React.createRef<HTMLInputElement>();
  const toInputRef = React.createRef<HTMLInputElement>();
  const [currencyFromAmount, setCurrencyFromAmount] = useState(0);
  const [currencyToAmount, setCurrencyToAmount] = useState(0);

  const checkBalance = (amount: number, balance: number) => amount > balance;
  const insufficientFundsError = checkBalance(
    currencyFromAmount,
    exchangeFromBalance
  );
  const disableCurrencyExchange =
    exchangeToCurrency === exchangeFromCurrency ||
    insufficientFundsError ||
    !currencyFromAmount ||
    rateError;

  const exchangeValue = (value: number, rate: number) =>
    fixDecimalPlace(value * rate);

  const reverseExchangeValue = (value: number, rate: number) =>
    fixDecimalPlace(value / rate);

  useEffect(() => {
    dispatch(
      startPollingRateAction({
        from: exchangeFromCurrency.currencyCode,
        to: exchangeToCurrency.currencyCode,
      })
    );
    return () => {
      dispatch(stopPollingRateAction());
    };
  }, [dispatch, exchangeFromCurrency, exchangeToCurrency]);

  useEffect(() => {
    if (isElementActive(fromInputRef)) {
      setCurrencyToAmount(exchangeValue(currencyFromAmount, rate));
    }
    if (isElementActive(toInputRef)) {
      setCurrencyFromAmount(reverseExchangeValue(currencyToAmount, rate));
    }
  }, [currencyFromAmount, currencyToAmount, fromInputRef, rate, toInputRef]);

  const onCurrencyFromChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const value = Number(e.target.value);
    if (decimalCount(value) > 2) {
      return;
    }
    setCurrencyFromAmount(fixDecimalPlace(value));
  };

  const onCurrencyToChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = Number(e.target.value);
    if (decimalCount(value) > 2) {
      return;
    }
    setCurrencyToAmount(fixDecimalPlace(value));
  };

  const onSelectFromCurrency = (account: Account) => {
    dispatch(changeExchangeFromAction(account.currency));
  };

  const onSelectToCurrency = (account: Account) => {
    dispatch(changeExchangeToAction(account.currency));
  };

  const resetFields = () => {
    setCurrencyToAmount(0);
    setCurrencyFromAmount(0);
  };

  const filteredCurrencyAccounts = (currency: Currency): Account[] =>
    accounts.filter(
      (account) => account.currency.currencyCode !== currency.currencyCode
    );

  const exchange = () => {
    const payload = {
      fromCurrency: exchangeFromCurrency,
      fromBalance: exchangeFromBalance,
      fromAmount: currencyFromAmount,
      toCurrency: exchangeToCurrency,
      toBalance: exchangeToBalance,
      toAmount: currencyToAmount,
    };
    dispatch(updateAccountAction(payload));
    resetFields();
  };

  return (
    <Flex className={classes.root} testId="exchange-section">
      <Flex className={classes.topInfo}>
        <Text fontSize={28}>{`Sell ${exchangeFromCurrency.currencyCode}`}</Text>
        <Text
          fontSize={16}
        >{`Market Order. ${exchangeFromCurrency.currencySymbol}1 = ${exchangeToCurrency.currencySymbol}${rate}`}</Text>
        {rateError && (
          <Text color={theme.palette.error.main}>
            Error Fetching Conversion rate, try another base currency
          </Text>
        )}
      </Flex>

      <Flex>
        <InputField
          id="currencyFrom"
          name="currencyFrom"
          type="number"
          inputMode="decimal"
          label="Convert"
          innerRef={fromInputRef}
          error={insufficientFundsError}
          value={currencyFromAmount}
          placeholder="0"
          currency={exchangeFromCurrency.currencySymbol}
          message={`${
            exchangeFromCurrency.currencyCode
          } Balance: ${exchangeFromBalance.toLocaleString()}`}
          onChange={onCurrencyFromChange}
          className={classes.input}
          testId="exchange-from-input"
        />
        <CurrencySelect
          accounts={accounts}
          onSelect={onSelectFromCurrency}
          initial={exchangeFromCurrency}
          testId="exchange-from-select"
        />
      </Flex>
      <Flex className={classes.inputDivider}>
        <div className={classes.rateIndicator} />
        <Text fontSize={14}>{rate}</Text>
      </Flex>

      <Flex>
        <InputField
          id="currencyTo"
          name="currencyTo"
          type="number"
          inputMode="decimal"
          label="To"
          innerRef={toInputRef}
          value={currencyToAmount}
          placeholder="0"
          currency={exchangeToCurrency.currencySymbol}
          message={`${
            exchangeToCurrency.currencyCode
          } Balance: ${exchangeToBalance.toLocaleString()}`}
          onChange={onCurrencyToChange}
          className={classes.input}
          testId="exchange-to-input"
        />
        <CurrencySelect
          accounts={filteredCurrencyAccounts(exchangeFromCurrency)}
          onSelect={onSelectToCurrency}
          initial={exchangeToCurrency}
          testId="exchange-to-select"
        />
      </Flex>
      <Button
        label="convert"
        width="100%"
        className={classes.exchangeButton}
        onClick={exchange}
        disabled={disableCurrencyExchange}
        testId="exchange-button"
      >
        {`Sell ${exchangeFromCurrency.currencyCode} for ${exchangeToCurrency.currencyCode}`}
      </Button>
    </Flex>
  );
};
