import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Icon, IconProps } from './Icon';

export default {
  title: 'Atoms/Icon',
  component: Icon,
} as Meta;

const Template: Story<IconProps> = (args) => <Icon {...args} />;

export const USD = Template.bind({});
USD.args = {
  width: 250,
  height: 250,
  icon: 'usd',
};
