import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';
import { iconList } from './IconList';

const useStyles = makeStyles<Theme, IconProps>(() =>
  createStyles({
    root: {
      '& svg': {
        width: ({ width = 25 }) => width,
        height: ({ height = 25 }) => height,
      },
    },
  })
);

interface IconColor {
  type?: string;
  shade?: number;
}

export interface IconProps {
  icon: string;
  width?: number | string;
  height?: number | string;
  color?: IconColor;
  className?: string;
}

export const Icon: React.FC<IconProps> = (props) => {
  const { icon, className } = props;
  const classes = useStyles(props);
  const IconTag = iconList[icon || 'defaultIcon'];
  return (
    <span className={clsx(classes.root, className)}>
      <IconTag />
    </span>
  );
};
