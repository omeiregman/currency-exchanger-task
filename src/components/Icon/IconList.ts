import React from 'react';
import { ReactComponent as usd } from '../../assets/icons/usd.svg';
import { ReactComponent as eur } from '../../assets/icons/eur.svg';
import { ReactComponent as gbp } from '../../assets/icons/gbp.svg';
import { ReactComponent as arrowDown } from '../../assets/icons/arrowDown.svg';
import { ReactComponent as add } from '../../assets/icons/add.svg';
import { ReactComponent as defaultIcon } from '../../assets/icons/default.svg';

interface StringTMap<T> {
  [key: string]: T;
}

interface IconList extends StringTMap<React.FC> {}

export const iconList: IconList = {
  add,
  arrowDown,
  eur,
  gbp,
  usd,
  defaultIcon,
};
