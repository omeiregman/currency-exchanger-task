import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Account } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { AccountCard } from '../AccountCard/AccountCard';
import { Card } from '../Card/Card';
import { Icon } from '../Icon/Icon';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      width: 535,
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      [theme.breakpoints.down('xs')]: {
        width: '100%',
        flexWrap: 'nowrap',
        overflowX: 'scroll',
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        '&::-webkit-scrollbar': {
          width: 0.25,
          height: 1,
          padding: theme.spacing(2),
          background: 'transparent',
        },
        '&::-webkit-scrollbar-thumb': {
          background: theme.palette.primary.light,
        },
      },
    },
    card: {
      display: 'flex',
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(2),
      cursor: 'pointer',
    },
    addAccount: {
      alignItems: 'center',
      margin: theme.spacing(0, 'auto'),
      [theme.breakpoints.down('xs')]: {
        width: 125,
        justifyContent: 'center',
      },
    },
  })
);

export interface AccountSectionProps {
  accounts: Account[];
}

export const AccountSection: React.FC<AccountSectionProps> = ({ accounts }) => {
  const classes = useStyles();
  return (
    <Flex className={classes.root} testId="account-section">
      <>
        {accounts.map((account) => (
          <div className={classes.card} key={account.currency.currencyCode}>
            <AccountCard
              balance={account.balance}
              currency={account.currency.currency}
              currencyCode={account.currency.currencyCode}
              currencySymbol={account.currency.currencySymbol}
              currencyIcon={account.currency.currencyIcon}
            />
          </div>
        ))}
        <Card width={250} height={120} className={classes.card}>
          <Flex className={classes.addAccount}>
            <Icon icon="add" />
          </Flex>
        </Card>
      </>
    </Flex>
  );
};
