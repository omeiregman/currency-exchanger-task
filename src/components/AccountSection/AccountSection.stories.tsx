import React from 'react';
import { Story, Meta } from '@storybook/react';

import { AccountSection, AccountSectionProps } from './AccountSection';
import { mockAccounts } from '../../utils/mock';

export default {
  title: 'Organisms/AccountSection',
  component: AccountSection,
} as Meta;

const Template: Story<AccountSectionProps> = (args) => (
  <AccountSection {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  accounts: mockAccounts,
};
