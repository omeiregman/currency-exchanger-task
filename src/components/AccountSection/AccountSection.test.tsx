import { render, screen, cleanup } from '@testing-library/react';
import { AccountSection } from './AccountSection';
import { mockAccounts } from '../../utils/mock';

afterEach(() => {
  cleanup();
});

describe('Account Section test', () => {
  it('should render Account Section component', () => {
    render(<AccountSection accounts={mockAccounts} />);
    const AccountSectionComponent = screen.getByTestId('account-section');
    expect(AccountSectionComponent).toBeInTheDocument();
    expect(AccountSectionComponent).toHaveTextContent('Dollar');
  });
});
