import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Card } from '../Card/Card';
import { Flex } from '../Flex/Flex';
import { Icon } from '../Icon/Icon';
import { Currency } from '../../types/types';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: 12,
      cursor: 'pointer',
    },
    currency: {
      fontSize: 18,
      color: theme.palette.common.black,
      marginTop: 4,
      marginBottom: 4,
    },
    currencyCode: {
      fontSize: 16,
      color: theme.palette.secondary.main,
      textTransform: 'uppercase',
    },
    amount: {
      fontSize: 24,
      fontWeight: 500,
      color: theme.palette.common.black,
      marginTop: 8,
      marginBottom: 8,
    },
    currencyIcon: {
      marginLeft: 'auto',
    },
  })
);

export interface AccountCardProps extends Currency {
  balance: number;
}

export const AccountCard: React.FC<AccountCardProps> = ({
  currency,
  currencyCode,
  currencySymbol,
  currencyIcon,
  balance,
}) => {
  const classes = useStyles();
  return (
    <Card
      width={250}
      height={120}
      className={classes.root}
      testId={`account-card-${currencyCode}`}
    >
      <Flex flexDirection="column">
        <div className={classes.currency}>{currency}</div>
        <div className={classes.currencyCode}>{currencyCode}</div>
        <div
          className={classes.amount}
        >{`${currencySymbol}${balance.toLocaleString()}`}</div>
      </Flex>
      <Flex className={classes.currencyIcon}>
        <Icon icon={currencyIcon} width={35} height={35} />
      </Flex>
    </Card>
  );
};
