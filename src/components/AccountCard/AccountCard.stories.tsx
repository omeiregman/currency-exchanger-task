import React from 'react';
import { Story, Meta } from '@storybook/react';

import { AccountCard, AccountCardProps } from './AccountCard';

export default {
  title: 'Molecules/AccountCard',
  component: AccountCard,
} as Meta;

const Template: Story<AccountCardProps> = (args) => <AccountCard {...args} />;

export const UsdCard = Template.bind({});
UsdCard.args = {
  balance: 250,
  currency: 'US Dollar',
  currencyCode: 'USD',
  currencySymbol: '$',
  currencyIcon: 'usd',
};

export const EurCard = Template.bind({});
EurCard.args = {
  balance: 1234,
  currency: 'Euro',
  currencyCode: 'EUR',
  currencySymbol: '€',
  currencyIcon: 'eur',
};

export const GbpCard = Template.bind({});
GbpCard.args = {
  balance: 235,
  currency: 'British Pounds',
  currencyCode: 'GBP',
  currencySymbol: '£',
  currencyIcon: 'gbp',
};
