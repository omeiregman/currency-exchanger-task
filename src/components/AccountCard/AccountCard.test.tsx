import { render, screen, cleanup } from '@testing-library/react';
import { AccountCard } from './AccountCard';

afterEach(() => {
  cleanup();
});
describe('Account Card test', () => {
  it('should render Account card component', () => {
    render(
      <AccountCard
        balance={200}
        currency="Dollar"
        currencyCode="usd"
        currencySymbol="USD"
        currencyIcon="usd"
      />
    );
    const AccountCardComponent = screen.getByTestId('account-card-usd');
    expect(AccountCardComponent).toBeInTheDocument();
    expect(AccountCardComponent).toHaveTextContent('Dollar');
  });
});
