import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles<Theme, CardProps>((theme) =>
  createStyles({
    root: {
      width: ({ width }) => width,
      height: ({ height }) => height,
      borderRadius: 8,
      boxShadow: theme.shadows[2],
      boxSizing: 'border-box',
    },
  })
);

export interface CardProps {
  width: string | number;
  height: string | number;
  children: React.ReactNode;
  className?: string;
  testId?: string;
}

export const Card: React.FC<CardProps> = (props) => {
  const { children, className, testId } = props;
  const classes = useStyles(props);
  return (
    <div className={clsx(className, classes.root)} data-testid={testId}>
      {children}
    </div>
  );
};
