import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { theme } from './utils/theme';
import { Home } from './components/Pages/Home';

export const App = () => (
  <ThemeProvider theme={theme}>
    <Home />
  </ThemeProvider>
);
