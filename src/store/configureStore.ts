import { compose, createStore, applyMiddleware, AnyAction, Store } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import reducers, { AppState } from './reducers';
import { rootEpic } from './epic';

const epicMiddleware = createEpicMiddleware();

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = (
  initialStore?: AppState
): Store<AppState, AnyAction> => {
  const store = createStore(
    reducers,
    initialStore,
    composeEnhancers(applyMiddleware(epicMiddleware))
  );
  epicMiddleware.run(rootEpic);
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers').default;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
};

export default configureStore;
