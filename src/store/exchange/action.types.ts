import { Currency, ExchangeCurrency } from '../../types/types';

export const FETCH_RATE = 'FETCH_RATE';
export const FETCH_RATE_SUCCESS = 'FETCH_RATE_SUCCESS';
export const FETCH_RATE_FAILURE = 'FETCH_RATE_FAILURE';
export const START_POLLING_RATE = 'START_POLLING_RATE';
export const STOP_POLLING_RATE = 'STOP_POLLING_RATE';
export const CHANGE_EXCHANGE_FROM = 'CHANGE_EXCHANGE_FROM';
export const CHANGE_EXCHANGE_TO = 'CHANGE_EXCHANGE_TO';

export interface FetchRateAction {
  type: typeof FETCH_RATE;
  payload: ExchangeCurrency;
}

export interface FetchRateSuccessAction {
  type: typeof FETCH_RATE_SUCCESS;
  rate: number;
}

export interface FetchRateFailureAction {
  type: typeof FETCH_RATE_FAILURE;
}

export interface StartPollingRateAction {
  type: typeof START_POLLING_RATE;
  payload: ExchangeCurrency;
}

export interface StopPollingRateAction {
  type: typeof STOP_POLLING_RATE;
}

export interface ChangeExchangeFromAction {
  type: typeof CHANGE_EXCHANGE_FROM;
  payload: Currency;
}

export interface ChangeExchangeToAction {
  type: typeof CHANGE_EXCHANGE_TO;
  payload: Currency;
}

export type ActionTypes =
  | FetchRateAction
  | FetchRateSuccessAction
  | FetchRateFailureAction
  | StopPollingRateAction
  | StartPollingRateAction
  | ChangeExchangeFromAction
  | ChangeExchangeToAction;
