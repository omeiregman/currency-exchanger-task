import { Currency } from '../../types/types';
import { LoadingState } from '../../types/enums';
import {
  ActionTypes,
  FETCH_RATE,
  FETCH_RATE_FAILURE,
  FETCH_RATE_SUCCESS,
  CHANGE_EXCHANGE_FROM,
  CHANGE_EXCHANGE_TO,
} from './action.types';
import { mockAccounts } from '../../utils/mock';

export interface ExchangeReducerState {
  rate: number;
  exchangeFrom: Currency;
  exchangeTo: Currency;
  loading: LoadingState;
}

export const exchangeInitialState: ExchangeReducerState = {
  rate: 0,
  loading: LoadingState.PRISTINE,
  exchangeFrom: mockAccounts[0].currency,
  exchangeTo: mockAccounts[1].currency,
};

export const exchangeReducer = (
  state = exchangeInitialState,
  action: ActionTypes
) => {
  switch (action.type) {
    case FETCH_RATE: {
      return {
        ...state,
        loading: LoadingState.LOADING,
      };
    }
    case FETCH_RATE_SUCCESS: {
      return {
        ...state,
        loading: LoadingState.LOADED,
        rate: action.rate,
      };
    }
    case FETCH_RATE_FAILURE: {
      return {
        ...state,
        loading: LoadingState.FAILED,
        rate: 0,
      };
    }
    case CHANGE_EXCHANGE_FROM: {
      return {
        ...state,
        exchangeFrom: action.payload,
      };
    }
    case CHANGE_EXCHANGE_TO: {
      return {
        ...state,
        exchangeTo: action.payload,
      };
    }
    default:
      return {
        ...state,
      };
  }
};
