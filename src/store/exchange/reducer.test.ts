import { LoadingState } from '../../types/enums';
import { exchangeInitialState, exchangeReducer } from './reducer';
import {
  changeExchangeFromAction,
  changeExchangeToAction,
  fetchRateAction,
  fetchRateFailureAction,
  fetchRateSuccessAction,
} from './action';
import { mockAccounts } from '../../utils/mock';
import { Currency } from '../../types/types';

interface NewState {
  rate?: number;
  exchangeFrom?: Currency;
  exchangeTo?: Currency;
  loading?: LoadingState;
}

const getState = (newState: NewState) => ({
  ...exchangeInitialState,
  ...newState,
});

describe('Fetch Rate Reducer Test', () => {
  it('FETCH_RATE', () => {
    const initialState = getState({
      loading: LoadingState.PRISTINE,
    });
    const modifiedState = getState({
      loading: LoadingState.LOADING,
    });
    expect(
      exchangeReducer(initialState, fetchRateAction({ from: 'usd', to: 'eur' }))
    ).toEqual(modifiedState);
  });

  it('FETCH_RATE_SUCCESS', () => {
    const initialState = getState({
      loading: LoadingState.LOADING,
    });
    const modifiedState = getState({
      loading: LoadingState.LOADED,
      rate: 50,
    });
    expect(exchangeReducer(initialState, fetchRateSuccessAction(50))).toEqual(
      modifiedState
    );
  });

  it('FETCH_RATE_FAILURE', () => {
    const initialState = getState({
      loading: LoadingState.PRISTINE,
    });
    const modifiedState = getState({
      loading: LoadingState.FAILED,
    });
    expect(exchangeReducer(initialState, fetchRateFailureAction())).toEqual(
      modifiedState
    );
  });
});

describe('Exchange Reducer Test', () => {
  it('EXCHANGE_FROM', () => {
    const initialState = getState({});
    const modifiedState = getState({
      exchangeFrom: mockAccounts[0].currency,
    });
    expect(
      exchangeReducer(
        initialState,
        changeExchangeFromAction(mockAccounts[0].currency)
      )
    ).toEqual(modifiedState);
  });

  it('EXCHANGE_TO', () => {
    const initialState = getState({});
    const modifiedState = getState({
      exchangeTo: mockAccounts[1].currency,
    });
    expect(
      exchangeReducer(
        initialState,
        changeExchangeToAction(mockAccounts[1].currency)
      )
    ).toEqual(modifiedState);
  });
});
