import { AppState } from '../reducers';

export const getRateSelector = (state: AppState) => state.exchange.rate;

export const getExchangeFromSelector = (state: AppState) =>
  state.exchange.exchangeFrom;

export const getExchangeToSelector = (state: AppState) =>
  state.exchange.exchangeTo;

export const getRateLoadingStateSelector = (state: AppState) =>
  state.exchange.loading;
