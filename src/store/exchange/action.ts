import {
  ActionTypes,
  CHANGE_EXCHANGE_FROM,
  CHANGE_EXCHANGE_TO,
  FETCH_RATE,
  FETCH_RATE_FAILURE,
  FETCH_RATE_SUCCESS,
  STOP_POLLING_RATE,
  START_POLLING_RATE,
} from './action.types';
import { Currency, ExchangeCurrency } from '../../types/types';

export const fetchRateAction = (payload: ExchangeCurrency): ActionTypes => ({
  type: FETCH_RATE,
  payload,
});

export const fetchRateSuccessAction = (rate: number): ActionTypes => ({
  type: FETCH_RATE_SUCCESS,
  rate,
});

export const fetchRateFailureAction = (): ActionTypes => ({
  type: FETCH_RATE_FAILURE,
});

export const startPollingRateAction = (
  payload: ExchangeCurrency
): ActionTypes => ({
  type: START_POLLING_RATE,
  payload,
});

export const stopPollingRateAction = (): ActionTypes => ({
  type: STOP_POLLING_RATE,
});

export const changeExchangeFromAction = (payload: Currency): ActionTypes => ({
  type: CHANGE_EXCHANGE_FROM,
  payload,
});

export const changeExchangeToAction = (payload: Currency): ActionTypes => ({
  type: CHANGE_EXCHANGE_TO,
  payload,
});
