import axios from 'axios';
import { ActionsObservable } from 'redux-observable';
import { throwError } from 'rxjs';
import { AxiosMock } from '../../types/types';
import { mockRateApiResponse } from '../../utils/mock';
import {
  fetchRateAction,
  fetchRateFailureAction,
  fetchRateSuccessAction,
} from './action';
import { fetchRateEpic } from './epics';
import { FETCH_RATE } from './action.types';

jest.mock('axios');
const mockAxios = axios as AxiosMock;

describe('Fetch Rate test', () => {
  it('Fetches Rate success', () => {
    const request = { from: 'USD', to: 'EUR' };
    const response = mockAxios.mockResolvedValue(mockRateApiResponse);

    const expected = fetchRateSuccessAction(response[request.to]);
    const action$ = ActionsObservable.of(fetchRateAction(request));

    fetchRateEpic(action$).subscribe((action) => {
      expect(action.type).toBe(FETCH_RATE);
      expect(action).toEqual(expected);
    });
  });

  it('Fetches Rate failure', () => {
    const error = { error: 'Error' };
    const request = { from: 'USD', to: 'EUR' };
    mockAxios.mockRejectedValue(throwError(error));

    const expected = fetchRateFailureAction();
    const action$ = ActionsObservable.of(fetchRateAction(request));

    fetchRateEpic(action$).subscribe((action) => {
      expect(action).toEqual(expected);
    });
  });
});
