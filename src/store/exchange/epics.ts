import { ActionsObservable } from 'redux-observable';
import { from, of, timer } from 'rxjs';
import { isOfType } from 'typesafe-actions';
import {
  mergeMap,
  catchError,
  map,
  takeUntil,
  switchMap,
  filter,
} from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import {
  ActionTypes,
  FETCH_RATE,
  START_POLLING_RATE,
  StartPollingRateAction,
  STOP_POLLING_RATE,
  StopPollingRateAction,
} from './action.types';
import {
  fetchRateAction,
  fetchRateFailureAction,
  fetchRateSuccessAction,
} from './action';
import { api } from '../../services/base';
import { apiRoute, config } from '../../config';

export const fetchRateEpic = (action$: ActionsObservable<ActionTypes>) =>
  action$.pipe(
    filter(isOfType(FETCH_RATE)),
    mergeMap((action) =>
      from(
        api.get(
          `${apiRoute.rate}&base=${action.payload.from}&symbols=${action.payload.to}`
        )
      ).pipe(
        map((response: AxiosResponse) =>
          fetchRateSuccessAction(response.data.rates[action.payload.to])
        ),
        catchError(() => of(fetchRateFailureAction()))
      )
    )
  );

export const pollRateEpic = (
  action$: ActionsObservable<StartPollingRateAction | StopPollingRateAction>
) =>
  action$.pipe(
    filter(isOfType([START_POLLING_RATE, START_POLLING_RATE])),
    switchMap((action) =>
      timer(0, config.ratePollingFrequency).pipe(
        takeUntil(action$.ofType<StopPollingRateAction>(STOP_POLLING_RATE)),
        map(() => fetchRateAction(action.payload))
      )
    )
  );
