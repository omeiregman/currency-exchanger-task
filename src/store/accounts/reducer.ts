import { Account, AccountIndex } from '../../types/types';
import { LoadingState } from '../../types/enums';
import {
  ActionTypes,
  FETCH_ACCOUNTS,
  FETCH_ACCOUNTS_FAILURE,
  FETCH_ACCOUNTS_SUCCESS,
  UPDATE_ACCOUNT,
  UPDATE_ACCOUNT_FAILURE,
  UPDATE_ACCOUNT_SUCCESS,
} from './action.types';

export interface AccountReducerState {
  accounts: AccountIndex;
  loading: LoadingState;
}

export const accountInitialState: AccountReducerState = {
  accounts: {},
  loading: LoadingState.PRISTINE,
};

export const accountReducer = (
  state = accountInitialState,
  action: ActionTypes
) => {
  switch (action.type) {
    case FETCH_ACCOUNTS: {
      return {
        ...state,
        loading: LoadingState.LOADING,
      };
    }
    case FETCH_ACCOUNTS_SUCCESS: {
      return {
        ...state,
        loading: LoadingState.LOADED,
        accounts: action.accounts.reduce((acc: AccountIndex, curr: Account) => {
          acc[curr.currency.currencyCode] = curr;
          return acc;
        }, {}),
      };
    }
    case FETCH_ACCOUNTS_FAILURE: {
      return {
        ...state,
        loading: LoadingState.FAILED,
      };
    }
    case UPDATE_ACCOUNT: {
      return {
        ...state,
        loading: LoadingState.LOADING,
      };
    }
    case UPDATE_ACCOUNT_SUCCESS: {
      return {
        ...state,
        loading: LoadingState.LOADED,
        accounts: { ...state.accounts, ...action.payload },
      };
    }
    case UPDATE_ACCOUNT_FAILURE: {
      return {
        ...state,
        loading: LoadingState.FAILED,
      };
    }
    default:
      return {
        ...state,
      };
  }
};
