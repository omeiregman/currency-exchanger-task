import {
  ActionTypes,
  FETCH_ACCOUNTS,
  FETCH_ACCOUNTS_FAILURE,
  FETCH_ACCOUNTS_SUCCESS,
  UPDATE_ACCOUNT,
  UPDATE_ACCOUNT_FAILURE,
  UPDATE_ACCOUNT_SUCCESS,
} from './action.types';
import { Account, AccountIndex, Exchange } from '../../types/types';

export const fetchAccountsAction = (): ActionTypes => ({
  type: FETCH_ACCOUNTS,
});

export const fetchAccountsSuccessAction = (
  accounts: Account[]
): ActionTypes => ({
  type: FETCH_ACCOUNTS_SUCCESS,
  accounts,
});

export const fetchAccountsFailureAction = (): ActionTypes => ({
  type: FETCH_ACCOUNTS_FAILURE,
});

export const updateAccountAction = (payload: Exchange): ActionTypes => ({
  type: UPDATE_ACCOUNT,
  payload,
});

export const updateAccountSuccessAction = (
  payload: AccountIndex
): ActionTypes => ({
  type: UPDATE_ACCOUNT_SUCCESS,
  payload,
});

export const updateAccountFailureAction = (): ActionTypes => ({
  type: UPDATE_ACCOUNT_FAILURE,
});
