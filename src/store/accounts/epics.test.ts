import { ActionsObservable } from 'redux-observable';
import { mockAccounts } from '../../utils/mock';
import { fetchAccountsAction, fetchAccountsSuccessAction } from './action';
import { fetchAccountsEpic } from './epics';

describe('Fetch Accounts', () => {
  test('send correct actions when success', (done) => {
    const response = mockAccounts;

    const expected = fetchAccountsSuccessAction(response);
    const action$ = ActionsObservable.of(fetchAccountsAction());

    fetchAccountsEpic(action$).subscribe((action) => {
      expect(action).toEqual(expected);
      done();
    });
  });
});
