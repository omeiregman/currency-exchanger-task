import { AppState } from '../reducers';

export const getAccountsSelector = (state: AppState) =>
  Object.values(state.accounts.accounts);

export const getAccountsLoadingState = (state: AppState) =>
  state.accounts.loading;

export const getFromAccountBalance = (state: AppState) => {
  const { currencyCode } = state.exchange.exchangeFrom;
  return state.accounts.accounts[currencyCode].balance;
};

export const getToAccountBalance = (state: AppState) => {
  const { currencyCode } = state.exchange.exchangeTo;
  return state.accounts.accounts[currencyCode].balance;
};
