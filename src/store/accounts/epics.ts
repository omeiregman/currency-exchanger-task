import { ActionsObservable } from 'redux-observable';
import { isOfType } from 'typesafe-actions';
import { of, from } from 'rxjs';
import { mergeMap, catchError, map, filter } from 'rxjs/operators';

import {
  ActionTypes,
  FETCH_ACCOUNTS,
  UPDATE_ACCOUNT,
  UpdateAccountAction,
} from './action.types';
import {
  exchangeCurrencyService,
  getAccountsService,
} from '../../services/mock-services';
import {
  fetchAccountsFailureAction,
  fetchAccountsSuccessAction,
  updateAccountFailureAction,
  updateAccountSuccessAction,
} from './action';

export const fetchAccountsEpic = (action$: ActionsObservable<ActionTypes>) =>
  action$.pipe(
    filter(isOfType(FETCH_ACCOUNTS)),
    mergeMap(() =>
      from(getAccountsService()).pipe(
        map((response) => fetchAccountsSuccessAction(response)),
        catchError(() => of(fetchAccountsFailureAction()))
      )
    )
  );

export const updateAccountEpic = (
  action$: ActionsObservable<UpdateAccountAction>
) =>
  action$.pipe(
    filter(isOfType(UPDATE_ACCOUNT)),
    mergeMap((action) =>
      from(
        exchangeCurrencyService(
          action.payload.fromCurrency,
          action.payload.fromBalance,
          action.payload.fromAmount,
          action.payload.toCurrency,
          action.payload.toBalance,
          action.payload.toAmount
        )
      ).pipe(
        map((response) => updateAccountSuccessAction(response)),
        catchError(() => of(updateAccountFailureAction()))
      )
    )
  );
