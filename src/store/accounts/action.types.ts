import { Account, AccountIndex, Exchange } from '../../types/types';

export const FETCH_ACCOUNTS = 'FETCH_ACCOUNTS';
export const FETCH_ACCOUNTS_SUCCESS = 'FETCH_ACCOUNTS_SUCCESS';
export const FETCH_ACCOUNTS_FAILURE = 'FETCH_ACCOUNTS_FAILURE';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
export const UPDATE_ACCOUNT_SUCCESS = 'UPDATE_ACCOUNT_SUCCESS';
export const UPDATE_ACCOUNT_FAILURE = 'UPDATE_ACCOUNT_FAILURE';

export interface FetchAccountsAction {
  type: typeof FETCH_ACCOUNTS;
}

export interface FetchAccountsSuccessAction {
  type: typeof FETCH_ACCOUNTS_SUCCESS;
  accounts: Account[];
}

export interface FetchAccountsFailureAction {
  type: typeof FETCH_ACCOUNTS_FAILURE;
}

export interface UpdateAccountAction {
  type: typeof UPDATE_ACCOUNT;
  payload: Exchange;
}

export interface UpdateAccountSuccessAction {
  type: typeof UPDATE_ACCOUNT_SUCCESS;
  payload: AccountIndex;
}

export interface UpdateAccountFailureAction {
  type: typeof UPDATE_ACCOUNT_FAILURE;
}

export type ActionTypes =
  | FetchAccountsAction
  | FetchAccountsSuccessAction
  | FetchAccountsFailureAction
  | UpdateAccountAction
  | UpdateAccountSuccessAction
  | UpdateAccountFailureAction;
