import { accountInitialState, accountReducer } from './reducer';
import {
  mockAccountIndex,
  mockAccounts,
  mockUpdateAccountData,
} from '../../utils/mock';
import { LoadingState } from '../../types/enums';
import {
  fetchAccountsAction,
  fetchAccountsFailureAction,
  fetchAccountsSuccessAction,
  updateAccountAction,
  updateAccountFailureAction,
  updateAccountSuccessAction,
} from './action';
import { AccountIndex } from '../../types/types';

interface NewState {
  accounts?: AccountIndex;
  loading?: LoadingState;
}

const getState = (newState: NewState) => ({
  ...accountInitialState,
  ...newState,
});

describe('Fetch Accounts Reducer Test', () => {
  it('FETCH_ACCOUNT', () => {
    const initialState = getState({});
    const modifiedState = getState({
      loading: LoadingState.LOADING,
    });
    expect(accountReducer(initialState, fetchAccountsAction())).toEqual(
      modifiedState
    );
  });
});

describe('Fetch Accounts Success Reducer Test', () => {
  it('FETCH_ACCOUNT_SUCCESS', () => {
    const {
      currency: { currencyCode },
    } = mockAccounts[0];
    const initialState = getState({});
    const modifiedState = getState({
      loading: LoadingState.LOADED,
      accounts: { [currencyCode]: mockAccounts[0] },
    });
    expect(
      accountReducer(
        initialState,
        fetchAccountsSuccessAction([mockAccounts[0]])
      )
    ).toEqual(modifiedState);
  });
});

describe('Fetch Accounts Failure Reducer Test', () => {
  it('FETCH_ACCOUNT_FAILURE', () => {
    const initialState = getState({});
    const modifiedState = getState({
      loading: LoadingState.FAILED,
    });
    expect(accountReducer(initialState, fetchAccountsFailureAction())).toEqual(
      modifiedState
    );
  });
});

describe('Update account', () => {
  it('UPDATE_ACCOUNT', () => {
    const initialState = getState({
      loading: LoadingState.PRISTINE,
    });
    const modifiedState = getState({
      loading: LoadingState.LOADING,
    });
    expect(
      accountReducer(initialState, updateAccountAction(mockUpdateAccountData))
    ).toEqual(modifiedState);
  });

  it('UPDATE_ACCOUNT_SUCCESS', () => {
    const initialState = getState({});
    const modifiedState = getState({
      accounts: mockAccountIndex,
      loading: LoadingState.LOADED,
    });
    expect(
      accountReducer(initialState, updateAccountSuccessAction(mockAccountIndex))
    ).toEqual(modifiedState);
  });
  it('UPDATE_ACCOUNT_FAILURE', () => {
    const initialState = getState({
      loading: LoadingState.LOADING,
    });
    const modifiedState = getState({
      loading: LoadingState.FAILED,
    });
    expect(accountReducer(initialState, updateAccountFailureAction())).toEqual(
      modifiedState
    );
  });
});
