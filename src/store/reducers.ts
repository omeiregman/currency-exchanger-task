import { combineReducers } from 'redux';
import { AccountReducerState, accountReducer } from './accounts/reducer';
import { exchangeReducer, ExchangeReducerState } from './exchange/reducer';

export interface AppState {
  accounts: AccountReducerState;
  exchange: ExchangeReducerState;
}

const reducers = {
  accounts: accountReducer,
  exchange: exchangeReducer,
};

export default combineReducers<AppState>(reducers);
