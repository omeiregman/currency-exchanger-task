import { combineEpics } from 'redux-observable';
import { fetchAccountsEpic, updateAccountEpic } from './accounts/epics';
import { fetchRateEpic, pollRateEpic } from './exchange/epics';

export const rootEpic = combineEpics(
  fetchAccountsEpic,
  fetchRateEpic,
  pollRateEpic,
  updateAccountEpic
);
