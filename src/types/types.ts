import { AxiosStatic } from 'axios';

export interface Currency {
  currency: string;
  currencyCode: string;
  currencySymbol: string;
  currencyIcon: string;
}

export interface AccountIndex {
  [currencyCode: string]: Account;
}

export interface Account {
  balance: number;
  currency: Currency;
}

export interface ExchangeCurrency {
  from: string;
  to: string;
}

export interface Exchange {
  fromCurrency: Currency;
  fromBalance: number;
  fromAmount: number;
  toCurrency: Currency;
  toBalance: number;
  toAmount: number;
}

export interface Rate {
  string: number;
}

export interface RateResponse {
  base: string;
  disclaimer: string;
  license: string;
  rates: Rate;
  timestamp: number;
}

export interface AxiosMock extends AxiosStatic {
  mockResolvedValue: Function;
  mockRejectedValue: Function;
}
