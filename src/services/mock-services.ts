import { mockAccounts } from '../utils/mock';
import { Account, AccountIndex, Currency } from '../types/types';

export const getAccountsService = (): Promise<Account[]> =>
  new Promise((resolve, reject) => {
    try {
      resolve(mockAccounts);
    } catch (e) {
      reject(e);
    }
  });

export const exchangeCurrencyService = (
  fromCurrency: Currency,
  fromBalance: number,
  fromAmount: number,
  toCurrency: Currency,
  toBalance: number,
  toAmount: number
): Promise<AccountIndex> =>
  new Promise((resolve, reject) => {
    try {
      const newFromBalance = fromBalance - fromAmount;
      const newToBalance = toBalance + toAmount;
      const newAccountFrom = {
        currency: fromCurrency,
        balance: newFromBalance,
      };
      const newAccountTo = { currency: toCurrency, balance: newToBalance };

      const response = {
        [fromCurrency.currencyCode]: newAccountFrom,
        [toCurrency.currencyCode]: newAccountTo,
      };
      resolve(response);
    } catch (e) {
      reject(e);
    }
  });
