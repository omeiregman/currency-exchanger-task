import { exchangeCurrencyService, getAccountsService } from './mock-services';
import { mockAccounts } from '../utils/mock';

describe('get accounts service', () => {
  it('gets list of accounts', () => {
    expect.assertions(1);
    const response = getAccountsService();
    return expect(response).resolves.toEqual(mockAccounts);
  });
});

describe('exchange currency service', () => {
  const payload = {
    from: {
      currency: {
        currency: 'US Dollar',
        currencyCode: 'USD',
        currencySymbol: '$',
        currencyIcon: 'usd',
      },
      balance: 500,
    },
    to: {
      currency: {
        currency: 'Euro',
        currencyCode: 'EUR',
        currencySymbol: '€',
        currencyIcon: 'eur',
      },
      balance: 500,
    },
    fromAmount: 234,
    toAmount: 200,
  };
  const modifiedPayload = {
    from: {
      currency: {
        currency: 'US Dollar',
        currencyCode: 'USD',
        currencySymbol: '$',
        currencyIcon: 'usd',
      },
      balance: 266,
    },
    to: {
      currency: {
        currency: 'Euro',
        currencyCode: 'EUR',
        currencySymbol: '€',
        currencyIcon: 'eur',
      },
      balance: 700,
    },
  };
  it('should return correct balance', () => {
    expect.assertions(1);
    const output = {
      [payload.from.currency.currencyCode]: modifiedPayload.from,
      [payload.to.currency.currencyCode]: modifiedPayload.to,
    };
    const response = exchangeCurrencyService(
      payload.from.currency,
      payload.from.balance,
      payload.fromAmount,
      payload.to.currency,
      payload.to.balance,
      payload.toAmount
    );
    return expect(response).resolves.toEqual(output);
  });
});
