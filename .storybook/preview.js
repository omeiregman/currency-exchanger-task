import { ThemeProvider } from '@material-ui/core';
import { theme } from '../src/utils/theme';
import { Provider } from 'react-redux';

import createStore from '../src/store/configureStore';
const store = createStore();

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  (Story) => (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    </Provider>
  ),
];
